# TDD - Java

This is a starter project that assumes you are building a Java library
using Gradle, and JUnit for testing.

## Use

1. Fork this project, setting its name and slug.
2. [Remove fork relationship](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#unlink-a-fork)

## Developing

1. Open in GitPod
2.  In the "Gradle" terminal, wait until you see something like this
    ```
    BUILD SUCCESSFUL in 1m
    3 actionable tasks: 3 executed

    Waiting for changes to input files... (ctrl-d to exit)
    <-------------> 0% WAITING
    > IDLE
    > IDLE
    ```
    This will automatically update as you code.

Starter code is in `lib/src/main/java` and `lib/src/test/java`. Probably
the first thing you'll want to do is make your own packages and initial
test file.

Detailed test results available in `lib/build/reports/tests/test/index.html`.
Right-click in file tree and select "Open with Live Server"


## How I built this project

1. Created a project in GitLab.

2. Opened project in GitPod.

3. Installed JDK and Gradle.

    ```bash
    sdk install java 21.0.2-tem < /dev/null
    sdk install gradle 8.7 < /dev/null
    ```

4. Created a gradle project with mostly defaults (enter, enter, enter, ...)

    ```bash
    gradle init
    ```

5. Configured GitPod to run `./gradlew test --continuous`

    ```
    gp init
    vim .gitpod.yml     # view file for details
    ```
